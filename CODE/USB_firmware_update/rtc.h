#ifndef __RTC_H
#define __RTC_H

#include "LPC17xx.h"

typedef struct rtc_time
{
	int hour, min, sec;
	int day, dow, doy, mon, year;
}rtc_t;

#define		CCR_EN		0
#define		CCR_RESET	1
#define		CCR_CALIB	4

void rtc_init(rtc_t *rt);
void rtc_get(rtc_t *rt);

#endif


