#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/usb.h>
#include <linux/uaccess.h>

#define MIN(a,b) (((a) <= (b)) ? (a) : (b))
#define BULK_EP_OUT 0x01
#define BULK_EP_IN 0x82
#define MAX_PKT_SIZE 512

static struct usb_device *device;
static struct usb_class_driver class;
static unsigned char bulk_buf[MAX_PKT_SIZE];

static int pd_open(struct inode *i, struct file *f)
{
	printk(KERN_INFO "%s: pendrive is opened.\n", THIS_MODULE->name);
	return 0;
}
static int pd_close(struct inode *i, struct file *f)
{
	printk(KERN_INFO "%s: pendrive is closed.\n", THIS_MODULE->name);
	return 0;
}

static ssize_t pd_read(struct file *f, char __user *buf, size_t cnt, loff_t *off)
{
	int retval;
	int read_cnt, nbytes, bytes_to_read;
	unsigned int pipe;
	printk(KERN_INFO "%s: pendrive read operation invoked.\n", THIS_MODULE->name);

	/* Read the data from the bulk endpoint */
	pipe = usb_rcvbulkpipe(device, BULK_EP_IN);
	retval = usb_bulk_msg(device, pipe, bulk_buf, MAX_PKT_SIZE, &read_cnt, 5000);
	if (retval)
	{
		printk(KERN_ERR "%s: Bulk message returned %d\n", THIS_MODULE->name, retval);
		return retval;
	}
	bytes_to_read = MIN(cnt, read_cnt);
	nbytes = bytes_to_read - copy_to_user(buf, bulk_buf, bytes_to_read);
	return nbytes;
}

static ssize_t pd_write(struct file *f, const char __user *buf, size_t cnt, loff_t *off)
{
	int retval;
	int bytes_to_write, nbytes;
	unsigned int pipe;
	printk(KERN_INFO "%s: pendrive write operation invoked.\n", THIS_MODULE->name);

	bytes_to_write = MIN(cnt, MAX_PKT_SIZE);
	nbytes = bytes_to_write - copy_from_user(bulk_buf, buf, bytes_to_write);
	if (nbytes == 0)
	{
		printk(KERN_INFO "%s: no data to write.\n", THIS_MODULE->name);
		return -EFAULT;
	}

	/* Write the data into the bulk endpoint */
	pipe = usb_sndbulkpipe(device, BULK_EP_OUT);
	retval = usb_bulk_msg(device, pipe, bulk_buf, nbytes, &nbytes, 5000);
	if (retval)
	{
		printk(KERN_ERR "Bulk message returned %d\n", retval);
		return retval;
	}

	return nbytes;
}

static struct file_operations fops =
{
	.owner = THIS_MODULE,
	.open = pd_open,
	.release = pd_close,
	.read = pd_read,
	.write = pd_write,
};

static int pd_probe(struct usb_interface *interface, const struct usb_device_id *id)
{
	int i, retval;
	struct usb_host_interface *iface_desc;
	struct usb_endpoint_descriptor *endpoint;
	
	iface_desc = interface->cur_altsetting;
	printk(KERN_INFO "pd drive (%04X:%04X) plugged.\n", id->idVendor, id->idProduct);
	printk(KERN_INFO "pd drive interface: %d\n", iface_desc->desc.bInterfaceNumber);
	printk(KERN_INFO "pd drive bNumEndpoints: %02X\n", iface_desc->desc.bNumEndpoints);
	printk(KERN_INFO "pd drive bInterfaceClass: %02X\n", iface_desc->desc.bInterfaceClass);

	for (i = 0; i < iface_desc->desc.bNumEndpoints; i++)
	{
		endpoint = &iface_desc->endpoint[i].desc;

		printk(KERN_INFO "pd drive Endpoint [%d]->bEndpointAddress: 0x%02X\n", i, endpoint->bEndpointAddress);
		printk(KERN_INFO "pd drive Endpoint [%d]->bmAttributes: 0x%02X\n", i, endpoint->bmAttributes);
		printk(KERN_INFO "pd drive Endpoint [%d]->wMaxPacketSize: 0x%04X (%d)\n", i, endpoint->wMaxPacketSize, endpoint->wMaxPacketSize);
	}

	device = interface_to_usbdev(interface);

	class.name = "usb/pd%d";
	class.fops = &fops;
	if ((retval = usb_register_dev(interface, &class)) < 0)
		printk(KERN_ERR "Not able to get a minor for this device.");
	else
		printk(KERN_INFO "Minor obtained: %d\n", interface->minor);
	return retval;
}

static void pd_disconnect(struct usb_interface *interface)
{
	printk(KERN_INFO "pd drive interface %d now disconnected.\n", interface->cur_altsetting->desc.bInterfaceNumber);
	usb_deregister_dev(interface, &class);
}

/* Table of devices that work with this driver */
static struct usb_device_id id_table[] =
{
	{ USB_DEVICE(0xffff, 0x0004) },
	{} /* Terminating entry */
};
MODULE_DEVICE_TABLE (usb, id_table);

static struct usb_driver pd_driver =
{
	.name = "pd_driver",
	.probe = pd_probe,
	.disconnect = pd_disconnect,
	.id_table = id_table,
};

static int __init pd_init(void)
{
	int result;
	/* Register this driver with the USB subsystem */
	result = usb_register(&pd_driver);
	if (result)
		printk(KERN_ERR "usb_register failed. Error number %d", result);
	else
		printk(KERN_ERR "usb_register() successfully registered driver %d", result);
	return result;
}

static void __exit pd_exit(void)
{
	/* Deregister this driver with the USB subsystem */
	usb_deregister(&pd_driver);
	printk(KERN_ERR "usb_deregister() deregistered driver.");
}

module_init(pd_init);
module_exit(pd_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Nilesh Ghule <nilesh@sunbeaminfo.com>");
MODULE_DESCRIPTION("Basic USB device driver example for DESD Feb 2020 batch at Sunbeam Infotech");

/*
1. attach pendrive.
2. terminal> lsmod | grep "usb"
3. terminal> sudo modprobe -r uas
4. terminal> sudo modprobe -r usb_storage
5. terminal> make
6. terminal> sudo insmod usb_pd.ko
7. terminal> dmesg | tail -15
	* check if probe is invoked & device info is visible.
8. terminal> sudo cat /dev/pd0
	* Error: Invalid argument
	* dmesg | tail -15 --> check if open(), read() and close() is called.
9. terminal> sudo rmmod usb_pd.ko
*/
